<?php
use Laravel\Lumen\Routing\Router;
/** @var Router $router */

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return '';
});

// Register
$router->post('register', 'AuthController@register');
// Login
$router->post('login', 'AuthController@login');

$router->group(['middleware' => 'auth'], function () use ($router) {
    // Profile info
    $router->get('profile', 'UserController@profile');
    //Get one user by id
    $router->get('users/{id}', 'UserController@singleUser');
    // Get all user
    $router->get('users', 'UserController@allUsers');
});

$router->group(['middleware' => 'auth'], function (Router $router) {
    $router->get('test', [
        'as' => 'test.group',
        'uses' => 'ExampleController@functionName'
    ]);
});
