<?php
namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Controller;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use App\Models\User;

class UserController extends Controller
{
    /**
     * Instantiate a new UserController instance.
     *
     * @return void
     */
    public function __construct()
    {

    }

    /**
     * Get the authenticated User.
     *
     * @return JsonResponse
     */
    public function profile(): JsonResponse
    {
        return response()->json(['user' => Auth::user()], Response::HTTP_OK);
    }

    /**
     * Get all User.
     *
     * @return JsonResponse
     */
    public function allUsers(): JsonResponse
    {
        return $this->success(['users' => User::all()]);
    }

    /**
     * Get one user.
     *
     * @param $id
     * @return JsonResponse
     */
    public function singleUser($id): JsonResponse
    {
        try {
            $user = User::findOrFail($id);
            return response()->json(['user' => $user], Response::HTTP_OK);
        } catch (\Exception $e) {
            return response()->json(['message' => 'User not found!'], 404);
        }
    }

}
