<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Controller;
use App\Http\Resources\ExampleResource;
use Illuminate\Http\JsonResponse;

class ExampleController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Function demo
     * @return JsonResponse
     */
    public function functionName(): JsonResponse
    {
        $data = [
            'id' => 1,
            'title' => 'Demo'
        ];

        return $this->success($data);
    }
}
