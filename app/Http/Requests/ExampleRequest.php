<?php

namespace App\Http\Requests;

use Illuminate\Http\Request;

class ExampleRequest extends Request
{
    public function rules()
    {
        return [
            'user_id' => 'required|integer|min:1',
            'game_id' => 'required|integer|min:1',
            'character_id' => 'required',
            'server_id' => 'required'
        ];
    }

    public function messages()
    {
        return [
            'user_id.required' => 'User id không thể để trống',
            'user_id.integer' => 'User id phải là số tự nhiên',
            'user_id.min' => 'User id tối thiểu là 1',
            'game_id.required' => 'Game id không thể để trống',
            'game_id.integer' => 'Game id phải là số tự nhiên',
            'game_id.min' => 'Game id tối thiểu là 1',
            'character_id.required' => 'Id nhân vật không thể để trống',
            'server_id.required' => 'Id server không thể để trống'
        ];
    }
}
