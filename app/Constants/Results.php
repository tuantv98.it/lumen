<?php

namespace App\Constants;

class Results
{
    const CODE_OK = 0;
    const CODE_FAIL = 1;
    const CODE_SYSTEM_ERROR = 299;

    public static $message = [
        self::CODE_OK => 'Thành công',
        self::CODE_FAIL => 'Thất bại',
        self::CODE_SYSTEM_ERROR  => 'Không thể thực hiện được yêu cầu',
    ];

    public static function getMessage($code)
    {
        if (!empty(self::$message[$code])) {
            return self::$message[$code];
        }

        return '';
    }
}
