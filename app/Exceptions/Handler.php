<?php

namespace App\Exceptions;

use GuzzleHttp\Exception\ConnectException;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\URL;
use Illuminate\Validation\ValidationException;
use Laravel\Lumen\Exceptions\Handler as ExceptionHandler;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\HttpKernel\Exception\MethodNotAllowedHttpException;
use Throwable;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that should not be reported.
     *
     * @var array
     */
    protected $dontReport = [
        AuthorizationException::class,
        HttpException::class,
        ModelNotFoundException::class,
        ValidationException::class,
    ];

    /**
     * Report or log an exception.
     *
     * This is a great spot to send exceptions to Sentry, Bugsnag, etc.
     *
     * @param  \Throwable  $exception
     * @return void
     *
     * @throws \Exception
     */
    public function report(Throwable $exception)
    {
        $method = app('request')->getMethod();
        if ($exception instanceof HttpException) {
            Log::warning(get_class($exception) . ': (Code:' . $exception->getStatusCode() . ') ' . $exception->getMessage() . ' at ' . $exception->getFile() . ':' . $exception->getLine() . '([' . $method . '] Request: ' . URL::full() . ')');
            return;
        } else if ($exception instanceof ConnectException) {
            Log::alert('Call uri: ' . $exception->getRequest()->getUri() . ' get time out with exception: ' . $exception->getTraceAsString());
            return;
        }

        parent::report($exception);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Throwable  $exception
     * @return \Illuminate\Http\Response|\Illuminate\Http\JsonResponse
     *
     * @throws \Throwable
     */
    public function render($request, Throwable $exception)
    {
        if ($exception instanceof MethodNotAllowedHttpException) {
            return response()->json($exception->getMessage(), Response::HTTP_METHOD_NOT_ALLOWED);
        } elseif ($exception instanceof ValidationException && $exception->getResponse()) {
            $result = [
                'code' => Response::HTTP_UNPROCESSABLE_ENTITY,
                'success' => false,
                'status' => false,
                'message' => $exception->getResponse()
            ];

            return response()->json($result, Response::HTTP_UNPROCESSABLE_ENTITY);
        }

        return response()->json([
            'code' => Response::HTTP_INTERNAL_SERVER_ERROR,
            'success' => false,
            'status' => false,
            'message' => $exception->getMessage()
        ], Response::HTTP_INTERNAL_SERVER_ERROR);
    }
}
