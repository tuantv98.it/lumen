<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class RepositoryServiceProvider extends ServiceProvider
{
    /**
     * Register any repository application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
