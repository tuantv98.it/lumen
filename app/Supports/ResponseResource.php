<?php

namespace App\Supports;

use App\Constants\Results;
use Illuminate\Http\JsonResponse;
use Symfony\Component\HttpFoundation\Response;

trait ResponseResource
{
    /**
     * @param array $data
     * @param string $message
     * @param array $headers
     * @param int $options
     * @return JsonResponse
     */
    public function success(array $data = [], string $message = "success", array $headers = [], int $options = 0): JsonResponse
    {
        return $this->respond(Results::CODE_OK, false, $message, $data, $headers, $options);
    }

    /**
     * Response resource return
     * @param int $code
     * @param bool $error
     * @param string $message
     * @param array|null $data
     * @param array $headers
     * @param int $options
     * @return JsonResponse
     */
    public function respond(int $code = 0, bool $error = false, string $message = '', array $data = [], array $headers = [], int $options = 0): JsonResponse
    {
        $response = [
            'code' => $code,
            'message' => $message,
            'data' => $data,
        ];

        return response()->json($response, Response::HTTP_OK, $headers, $options);
    }

}
